/*
 *
 * Post reducer
 *
 */
import produce from 'immer';
import {
  GET_POSTS,
  GET_POSTS_SUCCESS,
  GET_POSTS_ERROR,
  ADD_POST,
  ADD_POST_SUCCESS,
  ADD_POST_ERROR,
} from './constants';

export const initialState = {
  listPosts: [],
  loading: false,
  error: null,
  message: '',
};

/* eslint-disable default-case, no-param-reassign */
const postReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case GET_POSTS:
        // draft.loading = true;
        break;
      case GET_POSTS_SUCCESS:
        draft.loading = false;
        draft.listPosts = action.listPosts;
        break;
      case ADD_POST_ERROR:
      case GET_POSTS_ERROR:
        draft.loading = false;
        draft.error = action.error;
        draft.message = 'Internal service error!!!';
        break;
      case ADD_POST:
        draft.loading = true;
        break;
      case ADD_POST_SUCCESS:
        draft.loading = false;
        draft.message = 'success';
        break;
    }
  });

export default postReducer;
