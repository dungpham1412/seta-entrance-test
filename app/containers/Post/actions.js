/*
 *
 * Post actions
 *
 */

import {
  GET_POSTS,
  GET_POSTS_SUCCESS,
  GET_POSTS_ERROR,
  ADD_POST,
  ADD_POST_SUCCESS,
  ADD_POST_ERROR,
} from './constants';

export function getPosts() {
  return {
    type: GET_POSTS,
  };
}

export function getPostsLoaded(data) {
  return {
    type: GET_POSTS_SUCCESS,
    listPosts: data,
  };
}

export function getPostsLoadingError(error) {
  return {
    type: GET_POSTS_ERROR,
    error,
  };
}

export function addPost(payload) {
  return {
    type: ADD_POST,
    payload,
  };
}

export function addPostLoaded() {
  return {
    type: ADD_POST_SUCCESS,
  };
}

export function addPostsLoadingError(error) {
  return {
    type: ADD_POST_ERROR,
    error,
  };
}
