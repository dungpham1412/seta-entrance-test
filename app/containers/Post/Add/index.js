/**
 *
 * PostAdd
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import styled from 'styled-components';

import makeSelectPost from '../selectors';
import { addPost } from '../actions';

const Form = styled.form`
  .invalid-feedback {
    display: block !important;
  }
`;

class PostAdd extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      formData: {
        id: '',
        userId: '',
        title: '',
        body: '',
      },
      errors: {},
    };
    this.onChangeInput = this.onChangeInput.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChangeInput(event) {
    const { name, value } = event.target;
    const newErrors = { ...this.state.errors };
    delete newErrors[name];
    this.setState(state => ({
      formData: {
        ...state.formData,
        [name]: value,
      },
      errors: newErrors,
    }));
  }

  onSubmit(event) {
    event.preventDefault();
    const { formData, errors } = this.state;
    const newErrors = { ...errors };
    Object.keys(formData).forEach(field => {
      if (!formData[field]) {
        newErrors[field] = `${field} is required`;
      } else {
        delete newErrors[field];
      }
    });
    if (!Object.keys(newErrors).length) {
      this.props.addNewPost(formData);
    } else {
      this.setState({
        errors: newErrors,
      });
    }
  }

  render() {
    const { formData, errors } = this.state;
    return (
      <Form>
        <div className="form-group">
          <label htmlFor="post-id">ID</label>
          <input
            className="form-control"
            placeholder="Enter id"
            name="id"
            id="post-id"
            value={formData.id}
            onChange={this.onChangeInput}
          />
          <div className="invalid-feedback">{errors.id}</div>
        </div>
        <div className="form-group">
          <label htmlFor="post-userId">User ID</label>
          <input
            className="form-control"
            placeholder="Enter user id"
            name="userId"
            id="post-userId"
            value={formData.userId}
            onChange={this.onChangeInput}
          />
          <div className="invalid-feedback">{errors.userId}</div>
        </div>
        <div className="form-group">
          <label htmlFor="post-title">Title</label>
          <input
            className="form-control"
            placeholder="Enter title"
            name="title"
            id="post-title"
            value={formData.title}
            onChange={this.onChangeInput}
          />
          <div className="invalid-feedback">{errors.title}</div>
        </div>
        <div className="form-group">
          <label htmlFor="post-body">Body</label>
          <textarea
            className="form-control"
            name="body"
            rows="4"
            id="post-body"
            value={formData.body}
            onChange={this.onChangeInput}
          />
          <div className="invalid-feedback">{errors.body}</div>
        </div>
        <button
          type="submit"
          className="btn btn-primary"
          onClick={this.onSubmit}
        >
          SAVE
        </button>
      </Form>
    );
  }
}

PostAdd.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  addNewPost: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  post: makeSelectPost(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    addNewPost: () => {
      dispatch(addPost());
    },
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(PostAdd);
