/* eslint-disable react/no-array-index-key */
/**
 *
 * PostList
 *
 */

import React, { memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { injectIntl, intlShape } from 'react-intl';
import { compose } from 'redux';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import makeSelectPost from '../selectors';
import { getPosts } from '../actions';
import messages from '../messages';
import { columns } from '../constants';

const Wrapper = styled.div`
  border: 1px solid #e5e5e5;
  box-shadow: 5px 5px 5px #e5e5e5;
  border-radius: 5px;
  float: inherit;
  padding: 15px;
  table {
    thead tr {
      th:nth-child(1),
      th:nth-child(2),
      th:nth-child(5) {
        width: 10%;
      }
      th:nth-child(3) {
        width: 20%;
      }
    }
  }
  .btn-add {
    margin-bottom: 10px;
  }
`;
export function PostList(props) {
  const {
    postsProps: { listPosts, loading },
    callRequestGetPosts,
    intl: { formatMessage },
  } = props;

  useEffect(() => {
    callRequestGetPosts();
  }, []);

  return (
    <Wrapper>
      {loading && (
        <div className="background-loader">
          <div className="loader" />
        </div>
      )}
      <Link to="/posts/add" className="btn btn-primary btn-add">
        ADD NEW POST
      </Link>
      <br />
      <table className="table table-bordered table-hover text-center">
        <thead>
          <tr>
            {columns.map((item, index) => (
              <th key={`post-th${index}`}>
                {formatMessage(messages[item.title])}
              </th>
            ))}
            <th>{formatMessage(messages.action)}</th>
          </tr>
        </thead>
        <tbody>
          {listPosts.map((item, idxTr) => (
            <tr key={`post-tr${idxTr}`}>
              {columns.map((col, idxTd) =>
                col.name === 'body' ? (
                  <td
                    dangerouslySetInnerHTML={{
                      __html: item[col.name],
                    }}
                    key={`post-td${idxTr}-${idxTd}`}
                  />
                ) : (
                  <td key={`post-td${idxTr}-${idxTd}`}>{item[col.name]}</td>
                ),
              )}
              <td>
                <Link to={`/posts/${item.id}`}>
                  <i className="far fa-edit item-icon" />
                </Link>
                <i className="far fa-trash-alt" />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </Wrapper>
  );
}

PostList.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  postsProps: PropTypes.object,
  callRequestGetPosts: PropTypes.func,
  intl: intlShape,
};

const mapStateToProps = createStructuredSelector({
  postsProps: makeSelectPost(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    callRequestGetPosts: () => {
      dispatch(getPosts());
    },
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
  injectIntl,
)(PostList);
