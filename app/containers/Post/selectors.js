/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectPostDomain = state => state.post || initialState;

const makeSelectPost = () =>
  createSelector(
    selectPostDomain,
    substate => substate,
  );

const makeSelectListPosts = () =>
  createSelector(
    selectPostDomain,
    postState => postState.listPosts,
  );

export { selectPostDomain, makeSelectListPosts };
export default makeSelectPost;
