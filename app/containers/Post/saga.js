/**
 * Gets the repositories of the user from Github
 */

import { call, put, takeLatest } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { ADD_POST, GET_POSTS } from './constants';
import {
  getPostsLoaded,
  getPostsLoadingError,
  addPostLoaded,
  addPostsLoadingError,
} from './actions';

import { getAllPosts, addNewPost } from '../../services/posts';

export function* getPosts() {
  try {
    const posts = yield call(getAllPosts);
    yield put(getPostsLoaded(posts));
  } catch (err) {
    yield put(getPostsLoadingError(err));
  }
}

export function* createPost(data) {
  try {
    yield call(addNewPost, data);
    yield put(addPostLoaded());
    yield put(push('/posts'));
  } catch (err) {
    yield put(addPostsLoadingError(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* postSaga() {
  yield takeLatest(GET_POSTS, getPosts);
  yield takeLatest(ADD_POST, createPost);
}
