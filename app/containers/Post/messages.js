/*
 * Post Messages
 *
 * This contains all the text for the Post container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.Post';

export default defineMessages({
  id: {
    id: `${scope}.id`,
    defaultMessage: 'ID',
  },
  user_id: {
    id: `${scope}.user_id`,
    defaultMessage: 'User ID',
  },
  title: {
    id: `${scope}.title`,
    defaultMessage: 'Title',
  },
  body: {
    id: `${scope}.body`,
    defaultMessage: 'Body',
  },
  action: {
    id: `${scope}.action`,
    defaultMessage: 'Action',
  },
});
