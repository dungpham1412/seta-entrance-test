/*
 *
 * Post constants
 *
 */

export const ADD_POST = 'app/Post/ADD_POST';
export const ADD_POST_ERROR = 'app/Post/ADD_POST_ERROR';
export const ADD_POST_SUCCESS = 'app/Post/ADD_POST_SUCCESS';

export const GET_POSTS = 'app/Post/GET_POSTS';
export const GET_POSTS_ERROR = 'app/Post/GET_POSTS_ERROR';
export const GET_POSTS_SUCCESS = 'app/Post/GET_POSTS_SUCCESS';

export const columns = [
  {
    name: 'id',
    title: 'id',
  },
  {
    name: 'userId',
    title: 'user_id',
  },
  {
    name: 'title',
    title: 'title',
  },
  {
    name: 'body',
    title: 'body',
  },
];
