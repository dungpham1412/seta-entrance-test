/**
 *
 * Post
 *
 */

import React, { memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Switch, Route } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import { message as popup } from 'antd';

import makeSelectPost from './selectors';
import reducer from './reducer';
import saga from './saga';
import List from './List';
import Add from './Add';

export function Post({ message }) {
  useInjectReducer({ key: 'post', reducer });
  useInjectSaga({ key: 'post', saga });

  useEffect(() => {
    if (message === 'success') {
      popup.success('creation successful');
    } else if (message) {
      popup.error(message);
    }
  }, [message]);

  return (
    <div>
      <Helmet>
        <title>Post</title>
        <meta name="description" content="Description of Post" />
      </Helmet>
      <Switch>
        <Route exact path="/posts" component={List} />
        <Route exact path="/posts/add" component={Add} />
      </Switch>
    </div>
  );
}

Post.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  message: PropTypes.string,
};

const mapStateToProps = createStructuredSelector({
  post: makeSelectPost(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(Post);
