import axios from 'axios';

const URL = 'https://jsonplaceholder.typicode.com/posts';

function getAllPosts() {
  return axios.get(URL).then(res => res.data);
}

function addNewPost(data) {
  return axios.post(URL, data).then(res => console.log(res));
}

export { getAllPosts, addNewPost };
